<?php

/**
 * @file
 * Plugin to handle the embedding of the contents of a variable into a panel.
 */

/**
 * Plugins are described by creating a $plugin array.
 *
 * This will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Variable content'),
  'single' => TRUE,
  'defaults' => array(
    'variable' => '',
    'links' => TRUE,
    'leave_node_title' => FALSE,
    'identifier' => '',
    'build_mode' => 'teaser',
  ),
  'title' => t('Content from variable'),
  'icon' => 'icon_content.png',
  'description' => t('Add content stored in a variable as a pane.'),
  'category' => t('Custom'),
  'top level' => TRUE,
  'edit form' => 'variable_content_edit_form',
  'render callback' => 'variable_content_render',
  'admin info' => 'variable__content_admin_info',
);

/**
 * Output function for the variable_content content type.
 *
 * Outputs a node based on the module and delta supplied in the configuration.
 */
function variable_content_render($subtype, $conf, $panel_args) {
  $var = variable_get($conf['variable']);

  $block = new stdClass();
  $block->content = check_markup($var['value'], $var['format']);
  return $block;
}

/**
 * The form to add or edit a variable_name as content.
 */
function variable_content_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  if ($form_state['op'] == 'add') {
    $form['variable'] = array(
      '#prefix' => '<div class="no-float">',
      '#title' => t('Enter the variable name'),
      '#type' => 'textfield',
      '#maxlength' => 512,
      '#weight' => -10,
      '#suffix' => '</div>',
    );
  }
  else {
    $form['variable'] = array(
      '#type' => 'value',
      '#value' => $conf['variable'],
    );
  }

  return $form;
}

/**
 * Validate the variable selection.
 */
function variable_content_edit_form_submit($form, &$form_state) {
  foreach (array('variable') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title for a variable.
 */
function variable_content_admin_title($subtype, $conf) {
  return $conf['variable'];
}

/**
 * Display the administrative information for a variable_content pane.
 */
function variable_content_admin_info($subtype, $conf) {
  // Just render the variable.
  return variable_content_render($subtype, $conf, array());
}
